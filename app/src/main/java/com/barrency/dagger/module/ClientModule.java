package com.barrency.dagger.module;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.barrency.config.preference.AppPreference;
import com.barrency.config.preference.PrefKey;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;


/*
  Created by Amir on 6/13/2018.
 */
@Module(includes = ContextModule.class)
public class ClientModule {
    @Provides
    public OkHttpClient client(HttpLoggingInterceptor interceptor, final Context context) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(chain -> {
                    Request.Builder request = chain.request().newBuilder()
                            .addHeader("Accept", "application/json")
                            .addHeader("Content-Type", "application/json");

                    String token = AppPreference.getInstance(context).getString(PrefKey.AUTH_TOKEN);
                    Log.d("tokennn",token);
                    if (token != null && !token.isEmpty()) {
                        request.addHeader("Authorization", token);
                    }
                    else
                        request.addHeader("Authorization","guest");

                    return chain.proceed(request.build());
                })
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Singleton
    @Provides
    public HttpLoggingInterceptor interceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }
}
