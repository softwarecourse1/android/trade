package com.barrency.dagger.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.barrency.config.AppData.BASE_URL;


/*
  Created by Amir on 6/13/2018.
 */
@Module(includes = ClientModule.class)
public class NetModule {
  @Singleton
  @Provides
  public Retrofit retrofit(OkHttpClient client, Gson gson) {
    return new Retrofit.Builder()
      .baseUrl(BASE_URL)
      .client(client)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .build();
  }

//2019-10-06T15:20:30.469Z
  //2019-11-23T14:47:49.4243719
  @Singleton
  @Provides
  public Gson gson() {
    return new GsonBuilder()
      .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
      .create();
  }
}
