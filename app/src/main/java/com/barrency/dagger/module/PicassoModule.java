package com.barrency.dagger.module;

import android.content.Context;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/*
  Created by Amir on 6/13/2018.
 */

@Module(includes = {ContextModule.class})
public class PicassoModule {

    @Provides
    @Singleton
    public Picasso picasso(Context context) {
        return new Picasso.Builder(context)
//      .downloader(downloader)
                .loggingEnabled(true)

                .build();
    }

//  @Provides
//
//  public OkHttp3Downloader downloader(OkHttpClient client) {
//    return new OkHttp3Downloader(client);
//  }
//
//  @Named("picasso")
//  @Provides
//  public OkHttpClient client() {
//    return new OkHttpClient();
//  }
}
