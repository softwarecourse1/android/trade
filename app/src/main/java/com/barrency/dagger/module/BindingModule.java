package com.barrency.dagger.module;

import android.content.Context;

import dagger.Module;

/*
  Created by hotmint on 2019-07-15.
 */
@Module
public class BindingModule {

    Context context;
//
    public BindingModule(Context context) {
        this.context = context;
    }


//    @Provides
//    @DataBinding // needs to be consistent with the component scope
//    public Picasso getPicasso() {
//        return new Picasso.Builder(context).build();
//    }
}
