package com.barrency.dagger.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/*
  Created by Amir on 6/13/2018.
 */

@Module
public class ContextModule {
  private Application application;

  public ContextModule(Application application) {
    this.application = application;
  }

  @Singleton
  @Provides
  public Context context() {
    return application.getApplicationContext();
  }

}
