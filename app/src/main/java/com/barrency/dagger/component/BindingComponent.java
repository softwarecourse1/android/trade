package com.barrency.dagger.component;


import androidx.databinding.DataBindingComponent;

import dagger.Component;
import com.barrency.dagger.DataBinding;
import com.barrency.dagger.module.BindingModule;

/*
  Created by hotmint on 2019-07-15.
 */
@DataBinding
@Component(dependencies = ApplicationComponent.class, modules = BindingModule.class)
public interface BindingComponent extends DataBindingComponent {

   // void inject(MainActivity inject);

}
