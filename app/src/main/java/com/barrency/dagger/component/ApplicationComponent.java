package com.barrency.dagger.component;

/*
  Created by Amir on 6/13/2018.
 */

import android.app.Application;

import com.google.gson.Gson;
import com.barrency.application.Base;
import com.barrency.dagger.module.AppModule;
import com.barrency.dagger.module.NetModule;
import com.barrency.dagger.module.PicassoModule;
import com.barrency.remote.BaseRepository;
import com.barrency.remote.UserRepository;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;


@Singleton
@Component(modules = {AppModule.class, NetModule.class, PicassoModule.class})
public interface ApplicationComponent {

    void inject(Base app);
    void inject(BaseRepository rep);
    void inject(UserRepository rep);


    Application application();
    HttpLoggingInterceptor httpLoggingInterceptor();
    Retrofit retrofit();

    Picasso picasso();
    Gson gson();
    OkHttpClient okHttpClient();


}
