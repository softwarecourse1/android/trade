package com.barrency.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/*
  Created by hotmint on 2019-07-15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DataBinding {
}
