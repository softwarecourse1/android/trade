package com.barrency.remote

import android.content.Context
import com.barrency.application.Base
import retrofit2.Retrofit
import javax.inject.Inject

/*
  Created by hotmint on 2020-03-24.
 */
open class BaseRepository(var context: Context) {

    @Inject
    lateinit var retrofit: Retrofit

    init {

        (context.applicationContext as Base).applicationComponent.inject(this)

    }

}