package com.barrency.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.barrency.R
import ir.hotmint.qpin.config.preference.IntentKey

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        activityResult(requestCode, resultCode, data)

    }
    private fun activityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intent = Intent(IntentKey.ACTIVITY_RESULT)
        // You can also include some extra data.
        intent.putExtra(IntentKey.DATA, data)
        intent.putExtra(IntentKey.RESULT_CODE, resultCode)
        intent.putExtra(IntentKey.REQUEST_CODE, requestCode)

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}
