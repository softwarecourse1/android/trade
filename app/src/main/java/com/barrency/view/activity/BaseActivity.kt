package com.barrency.view.activity

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/*
  Created by hotmint on 2020-03-26.
 */
open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }



    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (overrideConfiguration != null) {
            val uiMode: Int = overrideConfiguration.uiMode
            overrideConfiguration.setTo(baseContext.resources.configuration)
            overrideConfiguration.uiMode = uiMode
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }

    fun setAppLocale(localeCode: String) {
        val resources = (this as AppCompatActivity).resources
        val dm = resources.displayMetrics
        val config = resources.configuration
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(Locale(localeCode.toLowerCase()))
        } else {
            config.locale = Locale(localeCode.toLowerCase())
        }
        resources.updateConfiguration(config, dm)
    }

//    fun setAppLocale(localeCode: String) {
//        val locale = Locale(localeCode, "Ir")
//        val resources = getResources();
//        val dm = resources.getDisplayMetrics();
//        val config = resources.getConfiguration();
//        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//        config.setLocale(locale);
//        //  } else {
//        config.locale = locale;
//        //  }
//        //  if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
//        getApplicationContext().createConfigurationContext(config);
//        // } else {
//        resources.updateConfiguration(config, dm);
//        // }
//    }
}