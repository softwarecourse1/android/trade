package com.barrency.view.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import cn.pedant.SweetAlert.SweetAlertDialog
import com.barrency.BuildConfig
import com.barrency.config.preference.AppPreference
import com.barrency.config.preference.PrefKey

import com.barrency.remote.UserRepository
import com.barrency.view.fragment.ProgressDialogFragment


/*
  Created by hotmint on 2020-03-30.
 */
class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity()
    }

    fun startActivity() {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))

        finish()


    }



}



