package com.barrency.application;


import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;


import com.barrency.dagger.component.ApplicationComponent;
import com.barrency.dagger.component.DaggerApplicationComponent;
import com.barrency.dagger.module.AppModule;
import com.barrency.dagger.module.ContextModule;
import com.barrency.dagger.module.NetModule;
import com.barrency.dagger.module.PicassoModule;

import java.util.Locale;


/*
  Created by Amir on 12/11/2018.
 */
public class Base extends Application {
    public static int total;
    public static Context context;




    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
//        setAppLocale("fa");
        context = getApplicationContext();
        applicationComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .contextModule(new ContextModule(this))
                .picassoModule(new PicassoModule())
                .build();


    }


//    dataBindingComponent =

//    mPicassoComponent = DaggerPicassoComponent.builder()
//      .appModule(new AppModule(this))
//
//      .contextModule(new ContextModule(this))
//      .build();


    //setLocale();



    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }



    public void setAppLocale(String localeCode) {
        Locale locale = new Locale(localeCode,"Ir");
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
        config.setLocale(locale);
        // } else{
        config.locale=locale;
        //}
        //if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N){
        getApplicationContext().createConfigurationContext(config);
        //} else {
        resources.updateConfiguration(config,dm);
        // }

    }

}
