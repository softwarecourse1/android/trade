package com.barrency.config.preference;


public class PrefKey {

    // preference keys
    public static final String APP_PREFERENCE = "app_prefs";


    public static final String LANGUAGE = "Language";
    public static final String SYSTEM_TN = "stm";
    public static final String COUNTRY = "country";
    public static final String AUTH_TOKEN = "auth";
    public static final String GET_START = "start";

    public static final String AUTH_REFRESH_TOKEN = "refresh_token";




    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String USERNAME = "user_name";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String WIZARD = "WIZARD";
    public static final String CREDIT = "CREDIT";


    public static final String PROFILE_IMAGE = "profile_image";
    public static final String TOKEN_EXPIRE_DATE = "token_expire_date";




}
