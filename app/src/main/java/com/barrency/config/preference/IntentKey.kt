package ir.hotmint.qpin.config.preference

/*
  Created by hotmint on 2019-11-19.
 */
class IntentKey {
    companion object {
        const val AMOUNT = "amount"
        const val RESULT_CODE = "resultCode"
        const val ACTIVITY_RESULT = "ACTIVITY_RESULT"
        const val DATA = "DATA"
        const val REQUEST_CODE = "REQUEST_CODE"


        const val RESULT_MESSAGE = "resultMessage"
        const val DATE = "date"
        const val REFERENCE_NO = "referenceNo"
        const val RECHARGE_TYPE = "rechargeType"
        const val RECHARGE_STATUS = "rechargeStatus"
        const val MOBILE = "mobile"
        const val VOUCHER = "voucher"
        const val DEDUCTED="deductedAmount"
        const val LOYLITY="loyalPoint"
        const val MOBILEOPERATOR="mobileOperator"
        const val MOBILEOPERATORID="mobileOperatorId"
        const val OFFER="offerCode"
        const val QUANTITY="quantity"
        const val SUMAMOUNT="sumAmount"


    }
}