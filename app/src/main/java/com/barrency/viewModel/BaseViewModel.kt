package com.barrency.viewModel

import android.graphics.Typeface
import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import com.barrency.R
import java.util.*

/*
  Created by hotmint on 2020-03-24.
 */
open class BaseViewModel : ViewModel(), Observable {

    protected fun errorSnack(view: View, message: String): Snackbar {

        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        val tv = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
        val ac = snackbar.view.findViewById<TextView>(R.id.snackbar_action)
        val typeface = Typeface.createFromAsset(view.context.assets, "fonts/IRANSans(FaNum).ttf")
        tv.typeface = typeface
        ac.typeface = typeface
        return snackbar

    }

    private val callbacks = PropertyChangeRegistry()

    override fun addOnPropertyChangedCallback(
        callback: Observable.OnPropertyChangedCallback
    ) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(
        callback: Observable.OnPropertyChangedCallback
    ) {
        callbacks.remove(callback)
    }

    /**
     * Notifies observers that all properties of this instance have changed.
     */
    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies observers that a specific property has changed. The getter for the
     * property that changes should be marked with the @Bindable annotation to
     * generate a field in the BR class to be used as the fieldId parameter.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }



//    var toolbarTitle: String? = ""
//        @Bindable get() = field
//        set(value) {
//            field = value
//            notifyPropertyChanged(BR.toolbarTitle)
//        }

    open fun onclickBack(view: View) {
        view.findNavController().navigateUp()
    }
    fun setAppLocale(localeCode: String) {
        val resources = (this as AppCompatActivity).resources
        val dm = resources.displayMetrics
        val config = resources.configuration
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(Locale(localeCode.toLowerCase()))
        } else {
            config.locale = Locale(localeCode.toLowerCase())
        }
        resources.updateConfiguration(config, dm)
    }


}
