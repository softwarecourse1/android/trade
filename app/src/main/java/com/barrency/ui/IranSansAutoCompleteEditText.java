package com.barrency.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.barrency.config.preference.AppPreference;
import com.barrency.config.preference.PrefKey;


/**
 * Created by amir on 5/18/16.
 */
public class IranSansAutoCompleteEditText extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {

  private Context context;
  private AttributeSet attrs;
  private int defStyle;

  public IranSansAutoCompleteEditText(Context context) {
    super(context);
    this.context = context;
    init();
  }

  public IranSansAutoCompleteEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    this.attrs = attrs;
    init();
  }

  public IranSansAutoCompleteEditText(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    this.context = context;
    this.attrs = attrs;
    this.defStyle = defStyle;
    init(defStyle);
  }

  private void init() {
    String lang = AppPreference.getInstance(context).getString(PrefKey.LANGUAGE);

    Typeface regularFontFA = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSans(FaNum).ttf");
    Typeface boldFontFA = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum)_Bold.ttf");
    Typeface regularFontEN = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSans.ttf");
    Typeface boldFontEN = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobileBold.ttf");
    Typeface currentTypeFace = this.getTypeface();
    if (lang.equals("fa")) {


      if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
        this.setTypeface(boldFontFA);
      } else {
        this.setTypeface(regularFontFA);
      }
    } else {

      if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
        this.setTypeface(boldFontEN);
      } else {
        this.setTypeface(regularFontEN);
      }

    }
  }

  private void init(int style) {

    String lang = AppPreference.getInstance(context).getString(PrefKey.LANGUAGE);


    Typeface regularFontFA = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSans(FaNum).ttf");
    Typeface boldFontFA = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum)_Bold.ttf");

    Typeface regularFontEN = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSans.ttf");
    Typeface boldFontEN = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobileBold.ttf");

    Typeface currentTypeFace = this.getTypeface();

    if (lang.equals("fa")) {


      if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
        this.setTypeface(boldFontFA, style);
      } else {
        this.setTypeface(regularFontFA, style);
      }
    } else {

      if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
        this.setTypeface(boldFontEN, style);
      } else {
        this.setTypeface(regularFontEN, style);
      }

    }


  }

  @Override
  public void setTypeface(Typeface tf, int style) {
    super.setTypeface(tf, style);
  }

  @Override
  public void setTypeface(Typeface tf) {
    super.setTypeface(tf);
  }

}
