package com.barrency.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.lang.reflect.Field;


/**
 * Created by amir on 5/18/16.
 */
public class IranSansTabLayout extends TabLayout {

  private Context context;
  private AttributeSet attrs;
  private int defStyle;

  public IranSansTabLayout(Context context) {
    super(context);
    this.context = context;
  }

  public IranSansTabLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    this.attrs = attrs;
  }

  public IranSansTabLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    this.context = context;
    this.attrs = attrs;
    this.defStyle = defStyle;
  }
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    try {
      if (getTabCount() == 0)
        return;
      Field field = TabLayout.class.getDeclaredField("mTabMinWidth");
      field.setAccessible(true);
      field.set(this, (int) (getMeasuredWidth() / (float) getTabCount()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void setupWithViewPager(ViewPager viewPager)
  {
    super.setupWithViewPager(viewPager);
    Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans(FaNum).ttf");
    this.removeAllTabs();

    ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

    for (int i = 0, count = viewPager.getAdapter().getCount(); i < count; i++) {
      Tab tab = this.newTab();
      this.addTab(tab.setText(viewPager.getAdapter().getPageTitle(i)));
      AppCompatTextView view = (AppCompatTextView) ((ViewGroup)slidingTabStrip.getChildAt(i)).getChildAt(1);
      view.setTypeface(typeface, Typeface.NORMAL);
    }
  }
}
