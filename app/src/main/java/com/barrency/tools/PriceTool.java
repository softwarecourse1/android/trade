package com.barrency.tools;

import java.text.DecimalFormat;

import com.barrency.R;
import com.barrency.application.Base;


/**
 * Created by Amir on 2/25/2018.
 * Phone : 09388118730
 * Email : Hotmint77@gmail.com
 */

public class PriceTool {
  //جدا کردن سه تا سه تا مبلغ
  public static String getAmount(String value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(Integer.valueOf(value))+ Base.context.getString(R.string.currency);
  }
  public static String getAmount(int value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value)+  Base.context.getString(R.string.currency);
  }
  public static String getAmount(Long value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value) +  Base.context.getString(R.string.currency);
  }
  public static String getAmount(float value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value)+  Base.context.getString(R.string.currency);
  }

  public static String getAmountNo(String value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(Integer.valueOf(value));
  }
  public static String getAmountNo(int value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value);
  }
  public static String getAmountNo(Long value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value);
  }
  public static String getAmountNo(float value) {
    DecimalFormat decimalFormat = new DecimalFormat(" #,### ");
    return decimalFormat.format(value);
  }
}
