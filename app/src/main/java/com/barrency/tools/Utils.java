package com.barrency.tools;

import android.content.Context;

import java.util.Random;

import com.barrency.config.preference.AppPreference;
import com.barrency.config.preference.PrefKey;


/*
  Created by User on 12/31/2018.
 */
public class Utils {
    /**
     * @param context for access to Preferences
     * @return unique clientTransactionId
     */

    public static String clientTransactionId(Context context) {
        AppPreference appPreference = AppPreference.getInstance(context);
        String stn = appPreference.getString(PrefKey.SYSTEM_TN);
        if (stn == null || stn.isEmpty() || stn.equals("999999999")) {
            stn = String.valueOf(new Random().nextInt(10000 - 60 + 1) + 60);
        }
        Integer sum = Integer.valueOf(stn) + 1;
        String out = sum.toString();
        appPreference.setString(PrefKey.SYSTEM_TN, out);
        return stn;
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s).replace(" ", "0");
    }


    public static String GatWayUrl(int gateWayId, String token) {

        return "https://api.qpin.ir/api/Credit/RedirectToPaymentGateway?paymentGatewayId=" + gateWayId + "&token=" + token;

    }
}
